Prototype of a location-based spontaneous public events app (think Snapchat for public events) made during the Garage48 Vunk 2016 hackathon. 

Contributors: Andreas Vija, Denis Kovalenko. Team also included: Tuomo Hopia, Oleh Stasula, Luiz Felipe Kohler.

September 2016